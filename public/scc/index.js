/*
// credit https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript

//Get a param
$.QueryString.param
//-or-
$.QueryString["param"]
//This outputs something like...
//"val"

//Get all params as object
$.QueryString
//This outputs something like...
//Object { param: "val", param2: "val" }

*/

const storageName = 'color_survey_scc';
const surveyName = 'scc'
const API_SURVEY = '/scc-surveys';
const SURVEY_JSON = './scc-survey.json';

var lastScrollTop = 0;

(function ($) {
  $.QueryString = (function (paramsArray) {
    let params = {};

    for (let i = 0; i < paramsArray.length; ++i) {
      let param = paramsArray[i].split('=', 2);

      if (param.length !== 2) continue;

      params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, ' '));
    }

    return params;
  })(window.location.search.substr(1).split('&'));
})(jQuery);

(function ($) {

  Survey.StylesManager.applyTheme('modern');

  var localSurveyStrings = {
    pagePrevText: 'Précédent',
    pageNextText: 'Suivant',
    completeText: 'Finaliser',
  };

  var retry;

  // Survey.surveyLocalization.locales["fr"] = localSurveyStrings;

  const alertUser = (error = undefined, survey = undefined) => {
    const delay = 10000;
    console.log(error);
    $.toast({
      text: `<h4>Une erreur est survenue lors de la sauvegarde.</h4>
      <p><i>Merci de vérifier votre connection à internet.</i></p>
      <p>Tentative dans quelques secondes.</p>
      <p><small>Error: ${error.responseJSON.message}</small></p>`,
      hideAfter: delay,
      stack: 1,
      showHideTransition: 'fade',
      position: 'top-right',
      bgColor: 'red',
      textColor: '#fff',
      icon: 'warning',
      // afterHidden: () => {
      //   if (survey) {
      //     saveSurveyData(survey);
      //   }
      // },
    });

    if (retry) {
      clearTimeout(retry);
    }

    if (survey) {
      retry = setTimeout(() => {
        saveSurveyData(survey);
      }, delay);
    }
  };

  let surveyId = undefined;

  const surveyInit = () => {
    $.get(SURVEY_JSON).then((json) => {
      window.survey = new Survey.Model(json);

      survey.locale = 'fr';

      Object.assign(survey, {
        pagePrevText: 'Précédent',
        pageNextText: 'Suivant',
        completeText: 'Finaliser',
      });

      // survey.onComplete.add(function (result) {
      //   document.querySelector("#surveyResult").textContent =
      //     "Result JSON:\n" + JSON.stringify(result.data, null, 3);
      // });

      survey.onPartialSend.add(function (survey) {
        saveSurveyData(survey);
      });
      survey.onComplete.add(function (survey, options) {
        saveSurveyData(survey, true);
      });

      survey.sendResultOnPageNext = true;
      var prevData = window.localStorage.getItem(storageName) || null;
      if (prevData) {
        var data = JSON.parse(prevData);

        if (data.surveyId) {
          surveyId = data.surveyId;
          delete data.surveyId;
        }

        survey.data = data;

        if (data.pageNo) {
          survey.currentPageNo = data.pageNo;
        }
      }
      $('#surveyElement').Survey({ model: survey });
    });
  };

  const surveySessionClear = () => {
    window.localStorage.removeItem(storageName);
    surveyId = undefined;
  };

  const saveSurveyData = (survey, doSurveySessionClear = false) => {
    var data = survey.data;
    data.pageNo = survey.currentPageNo;

    // user QuerySting var s to trace survey diffusion source
    var s = $.QueryString.s;
    if (s) {
      data._s = s;
    }

    if (surveyId) {
      window.localStorage.setItem(
        storageName,
        JSON.stringify(Object.assign({}, data, { surveyId }))
      );

      $.ajax({
        method: 'PUT',
        url: `${API_SURVEY}/${surveyId}`,
        data: {
          name: surveyName,
          json: data,
        },
      })
        .then((_) => {
          if (doSurveySessionClear) {
            surveySessionClear();
          }
        })
        .catch((error) => {
          alertUser(error, survey);
        });
    } else {
      $.post({
        url: `${API_SURVEY}/`,
        data: {
          name: surveyName,
          json: data,
        },
      })
        .then((resp) => {
          if (doSurveySessionClear) {
            surveySessionClear();
          } else {
            surveyId = resp.id;
            window.localStorage.setItem(
              storageName,
              JSON.stringify(Object.assign({}, data, { surveyId }))
            );
          }
        })
        .catch((error) => {
          // if failed to POST we still make a local save
          window.localStorage.setItem(storageName, JSON.stringify(data));
          alertUser(error, survey);
        });
    }
  };

  // var onScrollDebounced = _.throttle(function () {
  //   var st = $(document).scrollTop();
  //   if (
  //     st > lastScrollTop && // scrolling up
  //     $(document).scrollTop() > $('#surveyElement').offset().top + 100
  //   ) {
  //     $('.surveyReset').show();
  //   }
  //   lastScrollTop = st;
  // }, 1000);

  $(function () {
    surveyInit();

    // $(document).on('scroll', function () {
    //   onScrollDebounced();
    // });

    $('.surveyReset').on('click', function () {
      // $(this).hide();

      surveySessionClear();
      survey.clear(true, true);

      $('html, body').animate(
        {
          scrollTop: $('#surveyElement').offset().top,
        },
        400
      );
    });
  });
})(jQuery);
