// Initializes the `surveys` service on path `/surveys`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { SccSurveys } from './scc-surveys.class';
import createModel from '../../models/scc-surveys.model';
import hooks from './scc-surveys.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'scc-surveys': SccSurveys & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
  };

  // Initialize our service with any options it requires
  app.use('/scc-surveys', new SccSurveys(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('scc-surveys');

  service.hooks(hooks);
}
