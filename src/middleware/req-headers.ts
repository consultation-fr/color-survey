import { Request, Response, NextFunction } from 'express';

type ExtendedRequest = Request & {
  feathers?: any;
};

export default () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return (req: ExtendedRequest, res: Response, next: NextFunction) => {
    req.feathers = req.feathers ?? {};

    req.feathers.ip = req.ip; //|| req.headers['client-ip'] || req.headers['x-real-ip'] || req.headers.f;
    req.feathers.ips = req.ips;
    req.feathers.headers = req.headers;

    console.log('req.ip', req.feathers.ip);
    next();
  };
};
