import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import cors from 'cors';

import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
import express from '@feathersjs/express';
import socketio from '@feathersjs/socketio';

import { Application } from './declarations';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import authentication from './authentication';
import sequelize from './sequelize';
// Don't remove this comment. It's needed to format import lines nicely.

const app: Application = express(feathers());

// Load app configuration
app.configure(configuration());
app.set('trust proxy', true);
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet({ frameguard: false }));
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.options('*', cors());
// app.use(function (req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*'); // update to match the domain you will make the request from
//   res.header(
//     'Access-Control-Allow-Headers',
//     'Origin, X-Requested-With, Content-Type, Accept'
//   );
//   next();
// });

app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder

app.get('/', (req, res) => {
  res.status(302).redirect('/q/');
});

app.use('/', express.static(app.get('public')));

app.use(
  '/node_modules/',
  express.static(path.join(app.get('public'), '../node_modules/'))
);

// Set up Plugins and providers
app.configure(express.rest()).use((req, res, next) => {
  req.feathers = req.feathers || {};

  req.feathers.ip = req.ip;
  req.feathers.headers = req.headers;
  next();
});
app.configure(socketio());

app.configure(sequelize);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

export default app;
